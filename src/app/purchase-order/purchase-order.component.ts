import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.css']
})
export class PurchaseOrderComponent implements OnInit {

  totalCost = 0;
  numItems = 0;
  search = new FormControl('');

  items = [
    { item: 1,
      name: 'BOLIGRAFO TICHER BPKE13502-502',
      description: 'Lorem ipsum dolor sit amat, consectetur adipiscing elit. Mauris et mollis ipsum, id vestibulum eros. Praesent suscipit tellus eu ligula congue consectetur.',
      price: 80,
      img: 'http://images.officedepot.com.mx/IMGMexico/images/ME/sku/T1/000000005T1.gif'
    },
    { item: 2,
      name: 'BOLIGRAFO MEC GIRATORIO COCO',
      description: 'Morbi posuere nisl sapien, id cursus elit lacinia ac. Proin semper turpis eget blandit efficitur. Praesent porttitor finibus risus. Mauris lacus ante.',
      price: 75,
      img: 'http://images.officedepot.com.mx/IMGMexico/images/ME/sku/T1/000000004T1.gif'
    },
    {
      item: 3,
      name: 'BOLIGRAFO BIC P-MED VERDE 12PK',
      description: 'Cras quis lacus arcu. Mauris vestibulum, leo ultrices convallis convallis, nisl sapien rhoncus augue, non tempus enim urna eget elit. Aliquam scelerisque ultricies justo, in ornare odio semper ac. Nulla quis arcu ex.',
      price: 50,
      img: 'http://images.officedepot.com.mx/IMGMexico/images/ME/sku/T1/000000006T1.gif'
    },
    {
      item: 4,
      name: 'BOLIGRAFO OFFICE DEPOT 4WC0015',
      description: 'Aenean pharetra est nec libero luctus imperdiet. Quisque euismod ex ornare justo luctus, sit amet porttitor sapien fermentum.',
      price: 10,
      img: 'http://images.officedepot.com.mx/IMGMexico/images/ME/sku/T1/000000002T1.gif'
    },
    {
      item: 5,
      name: 'BOLIGRAFO UB VISION ELITE .8MM',
      description: 'Quisque hendrerit at lacus id cursus. Suspendisse ornare nisl at dictum vulputate. Pellentesque blandit risus nec faucibus consequat. Donec pharetra risus non urna porttitor condimentum.',
      price: 30,
      img: 'http://images.officedepot.com.mx/IMGMexico/images/ME/sku/T1/000000001T1.gif'
    }
  ];

  itemsCopy = Object.assign([], this.items);

  itemsCart = [];

  constructor() { }

  ngOnInit() {
  }

  searchItems() {
    console.log('Searching items -> ', this.search.value);
  }

  addItemToCart(todo) {
    if(this.itemsCart.indexOf(todo) === -1) {
      this.itemsCart.push(todo);
      this.updateCartAdd(todo.price);
    }
  }

  deleteItemFromCart(complete) {
    this.itemsCart = this.itemsCart.filter(function (value, index, arr) {
      if (value.item === complete.item) { return false; } else { return true; }
    });
    this.updateCartDel(complete.price);
  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      return;
    }
    if (this.itemsCart.indexOf(this.items[event.previousIndex]) !== -1) {
      return;
    }
    transferArrayItem(event.previousContainer.data,
      event.container.data,
      event.previousIndex, event.currentIndex);

      this.items = this.itemsCopy;
      this.items = Object.assign([], this.itemsCopy);

      this.updateCartAdd(this.items[event.previousIndex]['price']);
  }

  updateCartAdd(price: number) {
    this.numItems ++;
    this.totalCost += price;
  }

  updateCartDel(price: number) {
    this.numItems --;
    this.totalCost -= price;
  }
}
