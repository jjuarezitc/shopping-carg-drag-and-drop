import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchItemsService {
  private searchItemEndpoint = '';

  constructor(private http: HttpClient) { }

  getProducts(q: string): Observable<any> {
    let endpoint = this.searchItemEndpoint.replace('Q', q);
    return this.http.post(endpoint, null);
  }
}
